#!/usr/bin/bash

DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DOCKER_PATH=/usr/src/myapp

docker run --rm                                                 \
           -u `id -u`                                           \
           -v "$DIRECTORY":$DOCKER_PATH                         \
           -w $DOCKER_PATH                                      \
           --network host                                       \
           openjdk:11 java                                      \
           --module-path libs                                   \
           -m be.sakisan.mcaber
