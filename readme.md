# building


Building is done with `$ make`. The necessary installed software are:
- make
- docker
- /usr/bin/find
- wget (just to download the JDA dependency)

# running the bot

After building, the bot can be started with `$ ./run-bot.sh`. The necessary config file is `mcaber.properties` in the same directory.

```
token=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
prefix=+
osu-api-key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
osu-web-session=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
solr-url=http://localhost:8983/solr/pp
```

For mac OS using docker: `solr-url=http://host.docker.internal:8983/solr/pp`
 
# testing

`$ make console` will bring up an interactive console where you can try out your commands. This doesn't require a valid bot token.
