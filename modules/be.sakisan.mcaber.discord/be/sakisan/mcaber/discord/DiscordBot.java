package be.sakisan.mcaber.discord;

import be.sakisan.mcaber.bot.Command;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiscordBot {
    private static final Logger log = LoggerFactory.getLogger(DiscordBot.class);
    private final String token;
    private final Command command;

    public DiscordBot(String token, Command command) {
        this.token = token;
        this.command = command;
    }

    public void run() throws Exception {
        try {
            JDA jda = new JDABuilder(this.token).addEventListener(
                new CommandListener(this.command)
            ).build();
            jda.awaitReady();
        } catch(InterruptedException e) {
            Thread.currentThread().interrupt();
            throw e;
        }
    }

}

