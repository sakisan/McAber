package be.sakisan.mcaber.discord;

import be.sakisan.mcaber.bot.Command;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class CommandListener
    extends ListenerAdapter {
    private final Command command;

    public CommandListener(Command command) {
        this.command = command;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot())
        return;
        // We don't want to respond to other bot accounts, including ourself
        Message message = event.getMessage();
        String content = message.getContentRaw(); // getContentRaw() is an atomic getter
        // getContentDisplay() is a lazy getter which modifies the content for e.g. console view (strip discord formatting)

        String response = this.command.response(content);
        if (response != null) {
            MessageChannel channel = event.getChannel();
            channel.sendMessage(response).queue(); // Important to call .queue() on the RestAction returned by sendMessage(...)
        }
    }

}

