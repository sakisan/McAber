package be.sakisan.mcaber;

import be.sakisan.mcaber.utils.StringUtils;
import be.sakisan.mcaber.utils.UnhandledException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
    private final String token;
    private final String prefix;
    private final String osuApiKey;
    private final String osuWebSession;
    private final String solrUrl;

    public Configuration() {
        try {
            Properties config = new Properties();
            config.load(new FileInputStream("mcaber.properties"));

            this.token = config.getProperty("token");

            String prefixProperty = config.getProperty("prefix");
            if (StringUtils.isEmpty(prefixProperty)) {
                prefixProperty = "!";
            }
            this.prefix = prefixProperty;

            this.osuApiKey = config.getProperty("osu-api-key");
            this.osuWebSession = config.getProperty("osu-web-session");
            this.solrUrl = config.getProperty("solr-url");
        } catch(IOException e) {
            throw new UnhandledException(e);
        }
    }

    public String getToken() {
        return this.token;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getOsuApiKey() {
        return this.osuApiKey;
    }

    public String getOsuWebSession() {
        return this.osuWebSession;
    }

    public String getSolrUrl() {
        return this.solrUrl;
    }

}

