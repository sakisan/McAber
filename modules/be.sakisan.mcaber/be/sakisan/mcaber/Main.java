package be.sakisan.mcaber;

import be.sakisan.mcaber.bot.Command;
import be.sakisan.mcaber.bot.CommandMap;
import be.sakisan.mcaber.bot.Console;
import be.sakisan.mcaber.bot.FixedResponse;
import be.sakisan.mcaber.bot.Prefix;
import be.sakisan.mcaber.discord.DiscordBot;
import be.sakisan.mcaber.osu.data.OsuData;
import be.sakisan.mcaber.osubot.GetOsuUser;
import be.sakisan.mcaber.osubot.Rebuild;
import be.sakisan.mcaber.osubot.Snipe;
import be.sakisan.mcaber.osurest.ApiData;
import be.sakisan.mcaber.osurest.SolrData;
import be.sakisan.mcaber.utils.StringUtils;

public class Main {

    public static void main(String[] args) throws Exception {
        new Main().run(args);
    }

    public void run(String[] args) throws Exception {
        Configuration config = new Configuration();
        boolean test = args.length > 0 && "test".equalsIgnoreCase(args[0]);
        String token = config.getToken();
        String prefix = config.getPrefix();

        CommandMap commands = commands(config);

        if (test) {
            new Console(commands).run();
        } else {
            if (StringUtils.isEmpty(token)) {
                System.out.println("Please fill in the configuration file");
            } else {
                new DiscordBot(token, new Prefix(prefix, commands)).run();
                System.out.println("bot is running");
            }
        }
    }

    public void printUsage() {
        System.out.println(
            "Usage: " + Main.class.getName() + " TOKEN [test|prod] [prefix]"
        );
    }

    private CommandMap commands(Configuration config) {
        final OsuData solrData = osuSolrData(config);
        final OsuData apiData = osuApiData(config);
        final CommandMap map = new CommandMap();
        final Command help = new CommandMap.Help(map);
        map.put("h", help);
        map.put("help", help);
        map.put("ping", new FixedResponse("Pong!"));
        map.put("user", new GetOsuUser(solrData));
        map.put("snipe", new Snipe(solrData));
        map.put("snipe!", new Snipe(apiData, solrData));
        map.put("rebuild", new Rebuild(apiData, solrData));
        return map;
    }

    private OsuData osuApiData(Configuration config) {
        return new ApiData(config.getOsuApiKey(), config.getOsuWebSession());
    }

    private OsuData osuSolrData(Configuration config) {
        return new SolrData(config.getSolrUrl(), osuApiData(config));
    }

}

