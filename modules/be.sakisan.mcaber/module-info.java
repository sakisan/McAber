module be.sakisan.mcaber {
    requires be.sakisan.mcaber.utils;
    requires be.sakisan.mcaber.bot;
    requires be.sakisan.mcaber.discord;
    requires be.sakisan.mcaber.osu;
    requires be.sakisan.mcaber.osubot;
    requires be.sakisan.mcaber.osurest;
}
