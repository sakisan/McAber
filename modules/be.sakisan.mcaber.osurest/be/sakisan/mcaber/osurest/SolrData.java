package be.sakisan.mcaber.osurest;

import be.sakisan.mcaber.data.DataMethods;
import be.sakisan.mcaber.osu.*;
import be.sakisan.mcaber.osu.data.MapScores;
import be.sakisan.mcaber.osu.data.OsuData;
import be.sakisan.mcaber.osu.data.OsuSpec;
import be.sakisan.mcaber.osu.data.StoreMapScores;
import be.sakisan.mcaber.utils.*;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SolrData implements OsuData {
    private final String solrUrl;
    private final OsuData backup;
    private final DataMethods methods;

    public SolrData(String solrUrl, OsuData backup) {
        this.solrUrl = solrUrl;
        this.backup = backup;
        this.methods = new DataMethods()
                .nullable(MapScores.class, this::getMapScores)
                .nullable(StoreMapScores.class, this::updateSolr)
        ;
    }

    public <D> Optional<D> getData(OsuSpec<D> spec) {
        return methods.applyMethod(spec)
                .or(() -> backup.getData(spec));
    }

    private HttpResponse<String> send(String url) {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder().uri(
                URI.create(url)
            ).build();
            return client.send(request, BodyHandlers.ofString());
        } catch(InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new UnhandledException(e);
        } catch(IOException e) {
            throw new UnhandledException(e);
        }
    }

    private List<Score> getMapScores(MapScores spec) {
        return getMapScoresById(spec.getBeatmapId());
    }

    private List<Score> getMapScoresById(int beatmap) {
        String url = this.solrUrl + "/select" + UrlUtils.query(
            List.of(
                Pair.of("fq", "map:" + beatmap),
                Pair.of("q", "*:*"),
                Pair.of("indent", "true"),
                Pair.of("wt", "json")
            )
        );
        HttpResponse<String> response = send(url);
        if (response.statusCode() != 200) {
            return Collections.emptyList();
        } else {
            String json = response.body();
            Json root = Json.read(json);
            List<Json> docs = root.at("response").at("docs").asJsonList();
            return ListUtils.map(docs, this::docToScore);
        }
    }

    private Score docToScore(Json doc) {
        Beatmap beatmap = new Beatmap(
            doc.at("map_name").asString(),
            doc.at("map").asInteger(),
            doc.at("mapset").asInteger(),
            doc.at("creator").asString(),
            getDate(doc.at("map_ranked_date").asString()),
            doc.at("ar").asFloat(),
            doc.at("cs").asFloat(),
            doc.at("bpm").asFloat(),
            doc.at("od").asFloat()
        );
        Player player = new Player(
            doc.at("player_name").asString(),
            doc.at("player").asString()
        );
        return new Score(
            player,
            beatmap,
            doc.has("score") ? doc.at("score").asInteger() : 0,
            getDate(doc.at("full_date").asString()),
            getMods(doc.at("mods").asList()),
            doc.at("accuracy").asFloat(),
            Rank.fromString(doc.at("rank").asString()),
            doc.at("combo").asInteger(),
            Mode.STANDARD,
            doc.at("pp").asFloat(),
            doc.at("id").asLong(),
            doc.has("maniakeys") ? doc.at("maniakeys").asInteger() : 0
        );
    }

    private Date getDate(String dateString) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(
            dateString,
            timeFormatter
        );
        return Date.from(Instant.from(offsetDateTime));
    }

    private String fromDate(Date date) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(
            ZoneId.of("UTC")
        );
        return timeFormatter.format(date.toInstant());
    }

    private List<Mod> getMods(List<Object> json) {
        return json.stream().filter(mod -> !mod.equals("None")).map(
            mod -> Mod.fromString(mod.toString())
        ).collect(Collectors.toList());
    }

    private void solrCommit() {
        String url = this.solrUrl + "/update" + UrlUtils.query(
            List.of(Pair.of("commit", "true"))
        );
        send(url);
    }

    private void deleteMap(int map, String mode) {
        String url = this.solrUrl + "/update" + UrlUtils.query(
            List.of(
                Pair.of(
                    "stream.body",
                    "<delete><query>map:" + map + " AND mode:" + mode + "</query></delete>"
                )
            )
        );
        send(url);
    }

    private void deleteScores(List<Score> scores) {
        if (scores != null && !scores.isEmpty()) {
            final String query = StringUtils.mapJoin(
                    scores,
                    score -> String.format(
                            "(mode:%s AND map:%d AND player:%s)",
                            score.getMode().name(),
                            score.getBeatmap().getId(),
                            score.getPlayer().getId()
                    ),
                    " OR "
            );
            String url = this.solrUrl + "/update" + UrlUtils.query(
                List.of(
                    Pair.of(
                        "stream.body",
                        "<delete><query>" + query + "</query></delete>"
                    )
                )
            );
            send(url);
        }
    }

    private Void updateSolr(StoreMapScores scores) {
        List<Score> scoresToUpdate = removeExistingScores(scores.getScores());
        if (scoresToUpdate != null && !scoresToUpdate.isEmpty()) {
            deleteScores(scoresToUpdate);
            String url = this.solrUrl + "/update";
            String json = toJson(scoresToUpdate);

            try {
                HttpClient client = HttpClient.newHttpClient();
                HttpRequest.Builder builder = HttpRequest.newBuilder();
                builder.uri(URI.create(url));
                builder.header("Content-Type", "application/json");
                builder.POST(BodyPublishers.ofString(json));
                HttpRequest request = builder.build();
                client.send(request, BodyHandlers.ofString());
            } catch(InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new UnhandledException(e);
            } catch(IOException e) {
                throw new UnhandledException(e);
            }
        }
        return null;
    }

    private List<Score> removeExistingScores(List<Score> scores) {
        final Stream<Integer> mapIds = scores.stream()
                .map(score -> score.getBeatmap().getId())
                .distinct();
        final Set<Long> scoreIds = mapIds.flatMap(map -> this.getMapScoresById(map).stream())
                .map(Score::getId)
                .collect(Collectors.toSet());
        return scores.stream()
                .filter(score -> !scoreIds.contains(score.getId()))
                .collect(Collectors.toList());
    }

    private String toJson(List<Score> scores) {
        Json json = Json.make(new ArrayList());
        for (Score score : scores) {
            json.asJsonList().add(toJson(score));
        }
        return json.toString();
    }

    private Json toJson(Score score) {
        Json json = Json.make(new HashMap());
        json.set("pp", score.getPP());
        json.set("pp_rounded", Math.floor(score.getPP()));
        json.set("country", "BE");
        json.set("mods", listToStrings(score.getMods()));
        json.set("accuracy", score.getAccuracy());
        json.set("type", "play");
        json.set("mapset", score.getBeatmap().getSetId());
        json.set("mode", score.getMode().name());
        json.set("uid", score.getMode().name() + "_" + score.getId());
        json.set("od", score.getOD());
        json.set("ar", score.getAR());
        json.set("bpm", score.getBPM());
        json.set("cs", score.getCS());
        json.set("legit", true);
        json.set("rank", score.getRank().name());
        json.set("id", score.getId());
        json.set("player_name", score.getPlayer().getName());
        json.set("player_name_kw", score.getPlayer().getName());
        json.set("map", score.getBeatmap().getId());
        json.set("date", fromDate(score.getDatePlayed()));
        json.set("full_date", fromDate(score.getDatePlayed()));
        json.set("player", score.getPlayer().getId());
        json.set("creator", score.getBeatmap().getCreator());
        json.set("map_name", score.getBeatmap().getName());
        json.set("map_name_kw", score.getBeatmap().getName());
        json.set("combo", score.getCombo());
        json.set("score", score.getScore());
        json.set(
            "map_ranked_date",
            fromDate(score.getBeatmap().getDateRanked())
        );
        json.set("last-indexed", fromDate(new Date()));
        if (score.getMode() == Mode.MANIA) {
            json.set("maniakeys", score.getManiaKeys());
        }
        return json;
    }

    private List<String> listToStrings(List<Mod> mods) {
        if (mods.isEmpty()) {
            return List.of("None");
        } else {
            return ListUtils.map(mods, Mod::name);
        }
    }

}

