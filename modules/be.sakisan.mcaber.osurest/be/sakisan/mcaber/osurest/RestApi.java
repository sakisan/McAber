package be.sakisan.mcaber.osurest;

import be.sakisan.mcaber.osu.Beatmap;
import be.sakisan.mcaber.osu.Player;
import be.sakisan.mcaber.utils.Json;
import be.sakisan.mcaber.utils.Pair;
import be.sakisan.mcaber.utils.UnhandledException;
import be.sakisan.mcaber.utils.UrlUtils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class RestApi {
    private static final String BASE_URL = "https://osu.ppy.sh/api/";
    private final String apiKey;

    public RestApi(String apiKey) {
        this.apiKey = apiKey;
    }

    private HttpResponse<String> send(String url) {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder().uri(
                URI.create(url)
            ).build();
            return client.send(request, BodyHandlers.ofString());
        } catch(InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new UnhandledException(e);
        } catch(IOException e) {
            throw new UnhandledException(e);
        }
    }

    public Player getPlayer(String name) {
        String url = BASE_URL + "get_user" + UrlUtils.query(
            List.of(
                Pair.of("k", this.apiKey),
                Pair.of("u", name),
                Pair.of("mode", "0"),
                Pair.of("type", "string")
            )
        );
        HttpResponse<String> response = send(url);
        if (response.statusCode() != 200) {
            return null;
        } else {
            String json = response.body();
            return jsonToPlayer(json);
        }
    }

    public Player getPlayer(int id) {
        String url = BASE_URL + "get_user" + UrlUtils.query(
            List.of(
                Pair.of("k", this.apiKey),
                Pair.of("u", String.valueOf(id)),
                Pair.of("mode", "0"),
                Pair.of("type", "id")
            )
        );
        HttpResponse<String> response = send(url);
        if (response.statusCode() != 200) {
            return null;
        } else {
            String json = response.body();
            return jsonToPlayer(json);
        }
    }

    public Beatmap getBeatmap(int id) {
        String url = BASE_URL + "get_beatmaps" + UrlUtils.query(
            List.of(Pair.of("k", this.apiKey), Pair.of("b", String.valueOf(id)))
        );
        HttpResponse<String> response = send(url);
        if (response.statusCode() != 200) {
            return null;
        } else {
            String json = response.body();
            return jsonToBeatmap(json);
        }
    }

    private Player jsonToPlayer(String json) {
        Json response = Json.read(json);
        Json info = response.asJsonList().get(0);
        String id = info.at("user_id").asString();
        String name = info.at("username").asString();
        return new Player(name, id);
    }

    private Beatmap jsonToBeatmap(String json) {
        Json response = Json.read(json);
        if (response.asJsonList().isEmpty()) {
            return null;
        } else {
            Json info = response.asJsonList().get(0);
            int status = info.at("approved").asInteger();
            if (status > 0) {
                return new Beatmap(
                    String.format(
                        "%s - %s [%s]",
                        info.at("artist").asString(),
                        info.at("title").asString(),
                        info.at("version").asString()
                    ),
                    info.at("beatmap_id").asInteger(),
                    info.at("beatmapset_id").asInteger(),
                    info.at("creator").asString(),
                    getDate(info.at("approved_date").asString()),
                    info.at("diff_approach").asFloat(),
                    info.at("diff_size").asFloat(),
                    info.at("bpm").asFloat(),
                    info.at("diff_overall").asFloat()
                );
            } else {
                return null;
            }
        }
    }

    public Date getDate(String dateString) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(
            "yyyy-MM-dd k:mm:ss"
        );
        LocalDateTime localDateTime = LocalDateTime.parse(
            dateString,
            timeFormatter
        );
        OffsetDateTime offsetDateTime = localDateTime.atOffset(ZoneOffset.UTC);
        return Date.from(Instant.from(offsetDateTime));
    }

}

