package be.sakisan.mcaber.osurest;

import be.sakisan.mcaber.osu.*;
import be.sakisan.mcaber.utils.Json;
import be.sakisan.mcaber.utils.ListUtils;
import be.sakisan.mcaber.utils.UnhandledException;

import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WebApi {
    private final String websession;

    public WebApi(String websession) {
        this.websession = websession;
    }

    public List<Score> getMapScores(Beatmap beatmap, Mode gameMode) {
        String url = "https://osu.ppy.sh/beatmaps/" + beatmap.getId()
                + "/scores?type=country&mode=" + getMode(gameMode);
        HttpResponse<String> response = send(url);
        return toScores(beatmap, response.body(), gameMode);
    }

    private String getMode(Mode gameMode) {
        return gameMode.select(new Mode.Switch<>(
                () -> "osu",
                () -> "taiko",
                () -> "fruits",
                () -> "mania"
        ));
    }

    private HttpResponse<String> send(String url) {
        try {
            CookieManager cookieHandler = new CookieManager();
            CookieStore store = cookieHandler.getCookieStore();
            HttpCookie cookie = new HttpCookie("osu_session", this.websession);
            cookie.setDomain("osu.ppy.sh");
            cookie.setPath("/");
            store.add(URI.create("https://osu.ppy.sh/"), cookie);
            CookieHandler.setDefault(cookieHandler);
            HttpClient client = HttpClient.newBuilder().cookieHandler(
                cookieHandler
            ).build();
            HttpRequest request = HttpRequest.newBuilder().uri(
                URI.create(url)
            ).build();
            return client.send(request, BodyHandlers.ofString());
        } catch(InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new UnhandledException(e);
        } catch(IOException e) {
            throw new UnhandledException(e);
        }
    }

    private List<Score> toScores(Beatmap beatmap, String json, Mode gameMode) {
        List<Score> scores = new ArrayList<>();
        Json response = Json.read(json).at("scores");

        int i = 0;
        int limit = response == null ? 0 : response.asList().size();
        while (scores.size() < limit) {
            Json elem = response.at(i);
            scores.add(
                new Score(
                    new Player(
                        elem.at("user").at("username").asString(),
                        elem.at("user_id").asString()
                    ),
                    beatmap,
                    elem.at("score").asInteger(),
                    getDate(elem.at("created_at").asString()),
                    getMods(elem.at("mods").asList()),
                    elem.at("accuracy").asFloat(),
                    Rank.fromString(elem.at("rank").asString()),
                    elem.at("max_combo").asInteger(),
                    gameMode,
                    elem.at("pp").isNull() ? 0 : elem.at("pp").asFloat(),
                    elem.at("id").asLong(),
                    0
                )
            );
            i++;
        }
        return scores;
    }

    public Date getDate(String dateString) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(
            dateString,
            timeFormatter
        );
        return Date.from(Instant.from(offsetDateTime));
    }

    public List<Mod> getMods(List<Object> json) {
        return ListUtils.map(json, mod -> Mod.fromString(mod.toString()));
    }

}

