package be.sakisan.mcaber.osurest;

import be.sakisan.mcaber.data.DataMethods;
import be.sakisan.mcaber.osu.Beatmap;
import be.sakisan.mcaber.osu.Mode;
import be.sakisan.mcaber.osu.Player;
import be.sakisan.mcaber.osu.Score;
import be.sakisan.mcaber.osu.data.*;

import java.util.List;
import java.util.Optional;

public class ApiData implements OsuData {
    private final RestApi rest;
    private final WebApi web;
    private final DataMethods methods;

    public ApiData(String apiKey, String websession) {
        this.rest = new RestApi(apiKey);
        this.web = new WebApi(websession);
        this.methods = new DataMethods()
                .nullable(PlayerByName.class, this::playerByName)
                .nullable(PlayerById.class, this::playerById)
                .nullable(BeatmapById.class, this::beatmapById)
                .nullable(MapScores.class, this::mapScores)
        ;
    }

    public <D> Optional<D> getData(OsuSpec<D> spec) {
        return methods.applyMethod(spec);
    }

    private Player playerByName(PlayerByName playerByName) {
        return this.rest.getPlayer(playerByName.getName());
    }

    private Player playerById(PlayerById playerById) {
        return this.rest.getPlayer(playerById.getId());
    }

    private Beatmap beatmapById(BeatmapById beatmapById) {
        return this.rest.getBeatmap(beatmapById.getId());
    }

    private List<Score> mapScores(MapScores spec) {
        Beatmap beatmap;
        if (spec.getBeatmap() != null) {
            beatmap = spec.getBeatmap();
        } else {
            beatmap = this.rest.getBeatmap(spec.getBeatmapId());
        }
        if (beatmap != null) {
            return this.web.getMapScores(beatmap, spec.getGameMode());
        } else {
            return null;
        }
    }

}

