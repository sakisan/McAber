module be.sakisan.mcaber.osurest {
    requires java.net.http;
    requires be.sakisan.mcaber.utils;
    requires be.sakisan.mcaber.osu;
    exports be.sakisan.mcaber.osurest;
}
