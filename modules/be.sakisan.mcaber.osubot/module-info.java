module be.sakisan.mcaber.osubot {
    requires be.sakisan.mcaber.utils;
    requires be.sakisan.mcaber.bot;
    requires be.sakisan.mcaber.osu;
    exports be.sakisan.mcaber.osubot;
}
