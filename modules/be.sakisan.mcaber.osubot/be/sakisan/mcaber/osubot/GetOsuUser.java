package be.sakisan.mcaber.osubot;

import be.sakisan.mcaber.bot.Command;
import be.sakisan.mcaber.bot.CommandParser;
import be.sakisan.mcaber.osu.Player;
import be.sakisan.mcaber.osu.data.OsuData;
import be.sakisan.mcaber.osu.data.PlayerByName;
import be.sakisan.mcaber.utils.StringUtils;

import java.util.Optional;

public class GetOsuUser implements Command {
    private final OsuData data;

    public GetOsuUser(OsuData data) {
        this.data = data;
    }

    public String response(String input) {
        if (new CommandParser().wantsHelp(input)) {
            return help(new CommandParser().getLead(input));
        } else {
            String name = new CommandParser().getWord(1, input);
            if (StringUtils.isEmpty(name)) {
                return "No name given";
            } else {
                Optional<Player> player = data.getData(new PlayerByName(name));
                return player.map(this::presentable).orElse(null);
            }
        }
    }

    public String help(String command) {
        return "Usage: " + command + " <username>\n" + "e.g.:  " + command + " sakisan\n" + "Prints the information about a player";
    }

    private String presentable(Player player) {
        return player.getName() + " id:" + player.getId();
    }

}

