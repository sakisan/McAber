package be.sakisan.mcaber.osubot;

import be.sakisan.mcaber.osu.Beatmap;
import be.sakisan.mcaber.osu.Mode;
import be.sakisan.mcaber.osu.Score;
import be.sakisan.mcaber.osu.data.MapScores;
import be.sakisan.mcaber.osu.data.OsuData;
import be.sakisan.mcaber.osu.data.StoreMapScores;
import be.sakisan.mcaber.utils.Utils;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

public class ScoreUpdater {
    private final OsuData sourceData;
    private final OsuData solrData;
    private final PrintStream log;

    public ScoreUpdater(OsuData sourceData, OsuData solrData, PrintStream log) {
        this.sourceData = sourceData;
        this.solrData = solrData;
        this.log = log;
    }

    public void updateMap(Beatmap beatmap, Mode gameMode, int errorCount) {
        LocalDateTime date = LocalDateTime.now().withNano(0);
        String time = date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        String retries = (errorCount > 0 ? " retry " + errorCount : "");
        print(time + "   " + beatmap.getId() + " " + gameMode.name() + retries + " ");
        try {
            List<Score> scores = this.sourceData.getData(new MapScores(beatmap, gameMode))
                    .orElse(Collections.emptyList());
            this.solrData.getData(new StoreMapScores(scores));
            println(" - number of scores: " + scores.size());
        } catch (Exception e) {
            // if (e.getMessage() != null
            //         && (e.getMessage().contains("Connection")
            //         || e.getMessage().contains("EOF reached"))) {
                if (errorCount >= 20) {
                    throw e;
                } else {
                    printStackTrace(e);
                    Utils.sleep(10000L * errorCount); // wait a bit longer every time
                    updateMap(beatmap, gameMode, errorCount + 1);
                }
            // } else {
            //     throw e;
            // }
        }
    }

    private void print(String s) {
        if (this.log != null) {
            this.log.print(s);
        }
    }

    private void println(String s) {
        if (this.log != null) {
            this.log.println(s);
        }
    }

    private void printStackTrace(Exception e) {
        if (this.log != null) {
            e.printStackTrace(this.log);
        }
    }

}
