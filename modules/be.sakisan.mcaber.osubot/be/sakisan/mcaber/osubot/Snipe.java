package be.sakisan.mcaber.osubot;

import be.sakisan.mcaber.bot.Command;
import be.sakisan.mcaber.bot.CommandParser;
import be.sakisan.mcaber.osu.Mod;
import be.sakisan.mcaber.osu.Mode;
import be.sakisan.mcaber.osu.Score;
import be.sakisan.mcaber.osu.data.OsuData;
import be.sakisan.mcaber.osu.data.MapScores;
import be.sakisan.mcaber.osu.data.StoreMapScores;
import be.sakisan.mcaber.utils.StringUtils;
import be.sakisan.mcaber.utils.Utils;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Snipe implements Command {
    private final OsuData sourceData;
    private final OsuData solrData;

    public Snipe(OsuData sourceData, OsuData solrData) {
        this.sourceData = sourceData;
        this.solrData = solrData;
    }

    public Snipe(OsuData data) {
        this(data, null);
    }

    public String response(String input) {
        if (new CommandParser().wantsHelp(input)) {
            return help(new CommandParser().getLead(input));
        } else {
            String map = new CommandParser().getWord(1, input);
            String mode = new CommandParser().getWord(2, input);
            final Mode gameMode = Mode.getMode(mode);
            if (StringUtils.isEmpty(map)) {
                return "No map given";
            } else {
                int beatmap;
                try {
                    beatmap = Integer.parseInt(map);
                } catch(java.lang.NumberFormatException e) {
                    return map + " is not a number";
                }
                List<Score> scores = this.sourceData.getData(new MapScores(beatmap, gameMode))
                        .orElse(Collections.emptyList());
                update(scores);
                if (scores.isEmpty()) {
                    return "No scores found";
                } else {
                    int limit = Math.min(8, scores.size());
                    return IntStream.range(0, limit).mapToObj(
                        i -> format(i, scores.get(i))
                    ).collect(Collectors.joining("\n"));
                }
            }
        }
    }

    private void update(List<Score> scores) {
        if (updateActif()) {
            this.solrData.getData(new StoreMapScores(scores));
        }
    }

    private boolean updateActif() {
        return this.solrData != null;
    }

    public String help(String command) {
        String usage = "Usage: " + command + " <beatmap> <mode>\n" + "e.g.:  " + command + " 53\n" + "Prints the top scores for a beatmap.";
        if (updateActif()) {
            usage += "\nThis command also updates the database.";
        }
        return usage;
    }

    private String format(int i, Score score) {
        MessageFormat message = new MessageFormat(
            // uses a dot for the thousands separator
            "{0,number}. {2}: {1,number}{4} on {3,date}",
            Locale.GERMANY
        );
        return message.format(
            new Object[] {
                i + 1,
                score.getScore(),
                score.getPlayer().getName(),
                score.getDatePlayed(),
                formatMods(score.getMods())
            }
        );
    }

    private String formatMods(List<Mod> mods) {
        if (mods.isEmpty()) {
            return "";
        } else {
            return " with " + StringUtils.mapJoin(mods, Mod::name, ", ");
        }
    }

}

