package be.sakisan.mcaber.osubot;

import be.sakisan.mcaber.bot.Command;
import be.sakisan.mcaber.bot.CommandParser;
import be.sakisan.mcaber.osu.Beatmap;
import be.sakisan.mcaber.osu.Mode;
import be.sakisan.mcaber.osu.data.BeatmapById;
import be.sakisan.mcaber.osu.data.OsuData;
import be.sakisan.mcaber.utils.UnhandledException;
import be.sakisan.mcaber.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Rebuild implements Command {
    private final OsuData sourceData;
    private final OsuData solrData;

    public Rebuild(OsuData sourceData, OsuData solrData) {
        this.sourceData = sourceData;
        this.solrData = solrData;
    }

    public String response(String command) {
        int lowestMapId = getLowestMapId(command);
        long before = System.currentTimeMillis();
        long checkpoint = before;
        List<Integer> lines = getLines(lowestMapId);
        final int total = lines.size();
        for (int i = 0; i < total; i++) {
            int beatmap = lines.get(i);
            Optional<Beatmap> beatmapData
                    = this.sourceData.getData(new BeatmapById(beatmap));
            if (beatmapData.isPresent()) {
                for (Mode mode : Mode.values()) {
                    new ScoreUpdater(sourceData, solrData, System.out)
                            .updateMap(beatmapData.get(), mode, 0);
                    sleepCheck(checkpoint);
                }
            } else {
                System.out.println("map not found: " + beatmap);
            }
            checkpoint = System.currentTimeMillis();
            System.out.println("ETA: " + eta(i, total, before, checkpoint)
                                       + "   " + i + "/" + total + "\n");
        }
        long duration = (System.currentTimeMillis() - before);
        return "duration: " + duration + "ms";
    }

    private String eta(int i, int total, long before, long checkpoint) {
        return Utils.humanReadableMilliseconds(
                ((checkpoint - before) / (i + 1)) * (total - (i + 1)));
    }

    private List<Integer> getLines(double lowestMapId) {
        List<Integer> lines = new ArrayList<>();
        Scanner scan = getScanner();
        while (scan.hasNextLine()) {
            String lineIn = scan.nextLine();
            int beatmap = Integer.parseInt(lineIn);
            if (beatmap >= lowestMapId) {
                lines.add(beatmap);
            }
        }
        return lines;
    }

    private void sleepCheck(long checkpoint) {
        int timeBetween = 1670;
        long now = System.currentTimeMillis();
        if (now - checkpoint < timeBetween) {
            Utils.sleep(timeBetween - (now - checkpoint));
        }
    }

    private Scanner getScanner() {
        try {
            return new Scanner(new File("ranked_beatmaps"));
        } catch(FileNotFoundException e) {
            throw new UnhandledException(e);
        }
    }

    private int getLowestMapId(String command) {
        String word = new CommandParser().getWord(1, command);
        if (word != null) {
            return Integer.parseInt(word);
        } else {
            return 0;
        }
    }

}

