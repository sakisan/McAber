package be.sakisan.mcaber.osu;

import be.sakisan.mcaber.utils.MapBuilder;
import be.sakisan.mcaber.utils.Triple;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum Mod {

    // None           = 0,
    // NoFail         = 1,
    NF,
    // Easy           = 2,
    EZ,
    // //NoVideo      = 4,
    // NV,
    // Touch Device   = 4
    TD,
    // Hidden         = 8,
    HD,
    // HardRock       = 16,
    HR,
    // SuddenDeath    = 32,
    SD,
    // floatTime     = 64,
    DT,
    // Relax          = 128,
    RX,
    // HalfTime       = 256,
    HT,
    // Nightcore      = 512,
    NC,
    // Flashlight     = 1024,
    FL,
    // Autoplay       = 2048,
    AU,
    // SpunOut        = 4096,
    SO,
    // Relax2         = 8192,  // Autopilot
    R2,
    // Perfect        = 16384,
    PF,
    // Key4           = 32768,
    K4,
    // Key5           = 65536,
    K5,
    // Key6           = 131072,
    K6,
    // Key7           = 262144,
    K7,
    // Key8           = 524288,
    K8,
    K9,
    // FadeIn         = 1048576,
    FI,
    // Random         = 2097152,
    RA,
    // LastMod        = 4194304,
    LM;


    public static List<Mod> fromBits(int bits) {
        List<Mod> result = new LinkedList<>();
        Mod[] mods = Mod.values();
        if (bits != 0) {
            int f = 1;
            for (int i = 0; i < mods.length; i++) {
                Mod mod = mods[i];
                int filteredBit = bits & f;
                if (filteredBit != 0) {
                    result.add(mod);
                }
                f *= 2;
            }
        }
        return result;
    }

    private static Map<String, Mod> alt = new MapBuilder<String, Mod>()
            .with("NV", TD)
            .with("4K", K4)
            .with("5K", K5)
            .with("6K", K6)
            .with("7K", K7)
            .with("8K", K8)
            .with("9K", K9)
            .build();

    public static Mod fromString(String code) {
        Mod result = alt.get(code);
        if (result != null) {
            return result;
        } else {
            for (Mod mod : Mod.values()) {
                if (mod.name().equalsIgnoreCase(code)) {
                    return mod;
                }
            }
        }
        System.out.println(code + " is not a known mod");
        return null;
    }

    private static float factorDT(List<Mod> mods) {
        if (mods.contains(DT)) {
            return 2 / 3;
        } else if (mods.contains(HT)) {
            return 4 / 3;
        } else {
            return 1;
        }
    }

    public static float approachRate(float mapAR, List<Mod> mods) {
        return approachRateFromTime(approachTimeWithMods(mapAR, mods));
    }

    private static float approachTimeWithMods(float mapAR, List<Mod> mods) {
        float approachRate;
        if (mods.contains(HR)) {
            approachRate = Math.min(10, mapAR * 1.3F);
        } else if (mods.contains(EZ)) {
            approachRate = mapAR / 2;
        } else {
            approachRate = mapAR;
        }
        return approachTime(approachRate) * factorDT(mods);
    }

    private static float approachRateFromTime(float milliseconds) {
        if (milliseconds > 1200) {
            return (1800 - milliseconds) / 120;
        } else {
            return (1200 - milliseconds) / 150 + 5;
        }
    }

    private static float approachTime(float ar) {
        if (ar < 5) {
            return 1800 - (ar * 120);
        } else {
            return 1200 - ((ar - 5) * 120);
        }
    }

    public static float overallDifficulty(float mapOD, List<Mod> mods) {
        return odFromHitWindow300(hitWindow300(mapOD, mods));
    }

    private static final Triple<Float, Float, Float> hitWindowSteps = Triple.of(160F, 100F, 40F);

    private static float odFromHitWindow300(float hitWindow300) {
        return difficultyRangeReverse(hitWindowSteps, hitWindow300 * 2);
    }

    private static float hitWindow300(float mapOD, List<Mod> mods) {
        float overallDifficulty;
        if (mods.contains(HR)) {
            overallDifficulty = Math.min(10, mapOD * 1.4F);
        } else if (mods.contains(EZ)) {
            overallDifficulty = mapOD / 2;
        } else {
            overallDifficulty = mapOD;
        }
        float hitWindow = hitWindowGreat(overallDifficulty) * factorDT(mods);
        return osuStableFix(hitWindow);
    }

    private static float hitWindowGreat(float overallDifficulty) {
        return difficultyRange(hitWindowSteps, overallDifficulty) / 2;
    }

    private static float difficultyRange(Triple<Float, Float, Float> steps, float difficulty) {
        final Float middle = steps.getMiddle();
        final Float max = steps.getRight();
        final Float min = steps.getLeft();
        if (difficulty > 5) {
            return middle + (max - middle) * (difficulty - 5) / 5;
        } else if (difficulty < 5) {
            return middle + (middle - min) * (5 - difficulty) / 5;
        } else {
            return middle;
        }
    }

    private static float difficultyRangeReverse(
            Triple<Float, Float, Float> steps,
            float difficulty
    ) {
        final Float middle = steps.getMiddle();
        final Float max = steps.getRight();
        final Float min = steps.getLeft();
        if (difficulty < middle) {
            return (5 * (difficulty - middle)) / (max - middle) + 5;
        } else if (difficulty > middle) {
            return (5 * (difficulty - middle)) / (middle - min) + 5;
        } else {
            return 5;
        }
    }

    private static float osuStableFix(float hitWindow) {
        // replicating the buggy values from osu:stable
        return Math.round(2 * hitWindow) / 2F;
    }

    public static float beatsPerMinute(float bpm, List<Mod> mods) {
        return bpm * factorDT(mods);
    }

    public static float circleSize(float mapCS, List<Mod> mods) {
        if (mods.contains(HR)) {
            return Math.min(10, mapCS * 1.4F);
        } else if (mods.contains(EZ)) {
            return mapCS / 2;
        } else {
            return mapCS;
        }
    }

}

