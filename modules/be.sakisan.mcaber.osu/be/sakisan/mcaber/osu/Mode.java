package be.sakisan.mcaber.osu;

import be.sakisan.mcaber.utils.MapBuilder;
import be.sakisan.mcaber.utils.Utils;

import java.util.Map;
import java.util.function.Supplier;

public enum Mode {

    STANDARD,
    TAIKO,
    CATCH_THE_BEAT,
    MANIA;

    private static Map<String, Mode> names = new MapBuilder<String, Mode>()
            .with("std", STANDARD)
            .with("s", STANDARD)
            .with("osu", STANDARD)
            .with("o", STANDARD)

            .with("taiko", TAIKO)
            .with("tai", TAIKO)
            .with("t", TAIKO)

            .with("catch", CATCH_THE_BEAT)
            .with("ctb", CATCH_THE_BEAT)
            .with("c", CATCH_THE_BEAT)
            .with("fruits", CATCH_THE_BEAT)
            .with("fruit", CATCH_THE_BEAT)

            .with("mania", MANIA)
            .with("m", MANIA)
            .build();

    public static Mode getMode(String name) {
        return names.getOrDefault(
                String.valueOf(name).toLowerCase(),
                Utils.lookup(Mode.values(), name).orElse(Mode.STANDARD)
        );
    }

    public static final class Switch<R> {
        private final Supplier<R> standard;
        private final Supplier<R> taiko;
        private final Supplier<R> catchTheBeat;
        private final Supplier<R> mania;

        public Switch(
                Supplier<R> standard,
                Supplier<R> taiko,
                Supplier<R> catchTheBeat,
                Supplier<R> mania
        ) {
            this.standard = standard;
            this.taiko = taiko;
            this.catchTheBeat = catchTheBeat;
            this.mania = mania;
        }


    }

    public <R> R select(Switch<R> switcher) {
        switch (this) {
            default:
            case STANDARD:
                return switcher.standard.get();
            case TAIKO:
                return switcher.taiko.get();
            case CATCH_THE_BEAT:
                return switcher.catchTheBeat.get();
            case MANIA:
                return switcher.mania.get();
        }
    }
}