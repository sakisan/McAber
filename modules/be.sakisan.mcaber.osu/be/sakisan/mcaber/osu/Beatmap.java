package be.sakisan.mcaber.osu;

import java.util.Date;

public class Beatmap {
    private final String name;
    private final int id;
    private final int setId;
    private final String creator;
    private final Date dateRanked;
    private final float AR;
    private final float CS;
    private final float BPM;
    private final float OD;

    public Beatmap(
        String name,
        int id,
        int setId,
        String creator,
        Date dateRanked,
        float AR,
        float CS,
        float BPM,
        float OD
    ) {
        this.name = name;
        this.id = id;
        this.setId = setId;
        this.creator = creator;
        this.dateRanked = dateRanked;
        this.AR = AR;
        this.CS = CS;
        this.BPM = BPM;
        this.OD = OD;
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }

    public int getSetId() {
        return this.setId;
    }

    public String getCreator() {
        return this.creator;
    }

    public Date getDateRanked() {
        return this.dateRanked;
    }

    public float getAR() {
        return this.AR;
    }

    public float getCS() {
        return this.CS;
    }

    public float getBPM() {
        return this.BPM;
    }

    public float getOD() {
        return this.OD;
    }

}

