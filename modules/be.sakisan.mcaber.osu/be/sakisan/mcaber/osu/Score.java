package be.sakisan.mcaber.osu;

import java.util.Date;
import java.util.List;

public class Score {
    private final Player player;
    private final Beatmap beatmap;
    private final int score;
    private final Date datePlayed;
    private final List<Mod> mods;
    private final float accuracy;
    private final Rank rank;
    private final int combo;
    private final Mode mode;
    private final float pp;
    private final long id;
    private final int maniaKeys;

    public Score(
        Player player,
        Beatmap beatmap,
        int score,
        Date datePlayed,
        List<Mod> mods,
        float accuracy,
        Rank rank,
        int combo,
        Mode mode,
        float pp,
        long id,
        int maniaKeys
    ) {
        this.player = player;
        this.beatmap = beatmap;
        this.score = score;
        this.datePlayed = datePlayed;
        this.mods = mods;
        this.accuracy = accuracy;
        this.rank = rank;
        this.combo = combo;
        this.mode = mode;
        this.pp = pp;
        this.id = id;
        this.maniaKeys = maniaKeys;
    }

    public Player getPlayer() {
        return this.player;
    }

    public Beatmap getBeatmap() {
        return this.beatmap;
    }

    public int getScore() {
        return this.score;
    }

    public Date getDatePlayed() {
        return this.datePlayed;
    }

    public List<Mod> getMods() {
        return this.mods;
    }

    public float getAccuracy() {
        return this.accuracy;
    }

    public Rank getRank() {
        return this.rank;
    }

    public int getCombo() {
        return this.combo;
    }

    public Mode getMode() {
        return this.mode;
    }

    public float getPP() {
        return this.pp;
    }

    public long getId() {
        return this.id;
    }

    public int getManiaKeys() {
        return maniaKeys;
    }

    public float getAR() {
        return Mod.approachRate(getBeatmap().getAR(), getMods());
    }

    public float getOD() {
        return Mod.overallDifficulty(getBeatmap().getOD(), getMods());
    }

    public float getBPM() {
        return Mod.beatsPerMinute(getBeatmap().getBPM(), getMods());
    }

    public float getCS() {
        return Mod.circleSize(getBeatmap().getCS(), getMods());
    }
}

