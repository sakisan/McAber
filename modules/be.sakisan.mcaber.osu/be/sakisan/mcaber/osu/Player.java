package be.sakisan.mcaber.osu;

public class Player {
    private final String name;
    private final String id;

    public Player(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

}

