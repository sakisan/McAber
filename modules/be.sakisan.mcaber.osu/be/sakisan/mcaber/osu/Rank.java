package be.sakisan.mcaber.osu;

public enum Rank {

    XH,
    X,
    SH,
    S,
    A,
    B,
    C,
    D,
    F;


    public static Rank fromString(String code) {
        for (Rank rank : Rank.values()) {
            if (rank.name().equalsIgnoreCase(code)) {
                return rank;
            }
        }
        return null;
    }

}

