package be.sakisan.mcaber.osu.data;

import be.sakisan.mcaber.osu.Player;

public final class PlayerById implements OsuSpec<Player> {
    private final int id;

    public PlayerById(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

}

