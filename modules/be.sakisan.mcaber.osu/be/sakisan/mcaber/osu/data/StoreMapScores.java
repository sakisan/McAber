package be.sakisan.mcaber.osu.data;

import be.sakisan.mcaber.osu.Score;

import java.util.List;

public final class StoreMapScores implements OsuSpec<Void> {
    private final List<Score> scores;

    public StoreMapScores(List<Score> scores) {
        this.scores = scores;
    }

    public List<Score> getScores() {
        return this.scores;
    }
}

