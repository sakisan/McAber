package be.sakisan.mcaber.osu.data;

import be.sakisan.mcaber.osu.Beatmap;

public final class BeatmapById implements OsuSpec<Beatmap> {
    private final int id;

    public BeatmapById(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

}

