package be.sakisan.mcaber.osu.data;

import java.util.Optional;

public interface OsuData {

    <D> Optional<D> getData(OsuSpec<D> spec);

}

