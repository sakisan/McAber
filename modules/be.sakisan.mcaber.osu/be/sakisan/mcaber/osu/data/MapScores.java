package be.sakisan.mcaber.osu.data;

import be.sakisan.mcaber.osu.Beatmap;
import be.sakisan.mcaber.osu.Mode;
import be.sakisan.mcaber.osu.Score;

import java.util.List;

public final class MapScores implements OsuSpec<List<Score>> {
    private final int beatmapId;
    private final Beatmap beatmap;
    private final Mode gameMode;

    public MapScores(int beatmapId, Mode gameMode) {
        this.beatmapId = beatmapId;
        this.gameMode = gameMode;
        this.beatmap = null;
    }

    public MapScores(Beatmap beatmap, Mode gameMode) {
        this.beatmap = beatmap;
        this.beatmapId = beatmap.getId();
        this.gameMode = gameMode;
    }

    public int getBeatmapId() {
        return this.beatmapId;
    }

    public Beatmap getBeatmap() {
        return this.beatmap;
    }

    public Mode getGameMode() {
        return this.gameMode;
    }

}

