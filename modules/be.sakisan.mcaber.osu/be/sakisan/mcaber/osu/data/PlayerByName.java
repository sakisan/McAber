package be.sakisan.mcaber.osu.data;

import be.sakisan.mcaber.osu.Player;

public final class PlayerByName implements OsuSpec<Player> {
    private final String name;

    public PlayerByName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}

