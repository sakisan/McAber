package be.sakisan.mcaber.bot;

import be.sakisan.mcaber.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class CommandMap implements Command {
    private final Map<String, Command> commands = new HashMap<>();

    public void put(String lead, Command command) {
        this.commands.put(standardize(lead), command);
    }

    public String response(String input) {
        String response = null;
        String lead = new CommandParser().getLead(input);
        Command command = this.commands.get(standardize(lead));
        if (command != null) {
            response = command.response(input);
        }
        return response;
    }

    public String standardize(String word) {
        return word.toLowerCase();
    }

    public String help() {
        return " The available commands are: " + StringUtils.join(commandNames(), ", ");
    }

    private Iterable<String> commandNames() {
        return this.commands.keySet().stream().sorted()::iterator;
    }

    public static class Help implements Command {
        private final CommandMap map;

        public Help(CommandMap map) {
            this.map = map;
        }

        public String response(String input) {
            return this.map.help();
        }

    }

}

