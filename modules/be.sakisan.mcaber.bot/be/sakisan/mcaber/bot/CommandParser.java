package be.sakisan.mcaber.bot;

public class CommandParser {

    public String getLead(String input) {
        return getWord(0, input);
    }

    public boolean wantsHelp(String input) {
        String word = getWord(1,input);
        return "help".equalsIgnoreCase(word) || "h".equals(word);
    }

    public String[] getWords(String input) {
        return input.split(" ");
    }

    public String getWord(int i, String input) {
        String word = null;
        String[] words = getWords(input);
        if (words.length >= i + 1) {
            word = words[i];
        }
        return word;
    }

}

