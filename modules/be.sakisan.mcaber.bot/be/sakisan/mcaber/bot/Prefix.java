package be.sakisan.mcaber.bot;

public class Prefix implements Command {
    private final String prefix;
    private final Command command;

    public Prefix(String prefix, Command command) {
        this.prefix = prefix;
        this.command = command;
    }

    public String response(String input) {
        if (input.startsWith(prefix)) {
            return this.command.response(input.substring(prefix.length()));
        } else {
            return null;
        }
    }

}

