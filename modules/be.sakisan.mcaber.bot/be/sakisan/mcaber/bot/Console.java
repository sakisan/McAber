package be.sakisan.mcaber.bot;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Console {
    private final Command command;

    public Console(CommandMap command) {
        command.put("quit", quitCommand());
        command.put("q", quitCommand());
        this.command = command;
    }

    private Command quitCommand() {
        return new FixedResponse("Bye", "terminates the console");
    }

    public void run() {
        Scanner scan = new Scanner(System.in);
        System.out.println(
            "Commands work without a prefix here.\nType 'q' or 'quit' to terminate."
        );
        boolean prompt = true;
        while (prompt) {
            System.out.print("> ");
            try {
                String lineIn = scan.nextLine();
                String lineOut = this.command.response(lineIn);
                if (lineOut != null) {
                    System.out.println(lineOut);
                }
                if ("q".equals(lineIn) || "quit".equals(lineIn)) {
                    prompt = false;
                }
            } catch(NoSuchElementException e) {
                prompt = false;
            }
        }
    }

}

