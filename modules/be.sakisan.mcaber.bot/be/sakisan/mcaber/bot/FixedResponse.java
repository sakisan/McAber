package be.sakisan.mcaber.bot;

public class FixedResponse implements Command {
    private final String content;
    private final String help;

    public FixedResponse(String content, String help) {
        this.content = content;
        this.help = help;
    }

    public FixedResponse(String content) {
        this(content, null);
    }

    public String response(String input) {
        if (new CommandParser().wantsHelp(input)) {
            return help();
        } else {
            return this.content;
        }
    }

    public String help() {
        if (this.help != null) {
            return this.help;
        } else {
            return "no help here";
        }
    }

}

