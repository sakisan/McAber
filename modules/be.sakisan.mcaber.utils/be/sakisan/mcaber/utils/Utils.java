package be.sakisan.mcaber.utils;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class Utils {

    private Utils() {
        throw new java.lang.IllegalStateException("Utility class");
    }

    public static void sleep(long l) {
        try {
            Thread.sleep(l);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
            throw new UnhandledException(ie);
        }
    }

    private static String getEnumName(String value) {
        return StringUtils.isNotEmpty(value) ?
                value.toUpperCase().trim().replaceAll("[^a-zA-Z]", "_")
                : null;
    }

    public static <E extends Enum<E>> Optional<E> lookup(E[] enumValues, String enumName) {
        E value = null;
        String name = getEnumName(enumName);
        int i = 0;
        while (i < enumValues.length && value == null) {
            E enumValue = enumValues[i];
            if (enumValue.name().equals(name)) {
                value = enumValue;
            }
            i++;
        }
        return Optional.ofNullable(value);
    }

    public static String humanReadableMilliseconds(long millis) {
        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        return String.format("%d days, %d hours, %d min, %d sec",
                             days, hours, minutes, seconds
        );
    }
}
