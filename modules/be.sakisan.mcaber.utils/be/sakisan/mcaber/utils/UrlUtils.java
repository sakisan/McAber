package be.sakisan.mcaber.utils;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;

public class UrlUtils {

    private UrlUtils() {
        throw new java.lang.IllegalStateException("Utility class");
    }

    public static String query(List<Pair<String, String>> parameters) {
        if (parameters == null || parameters.isEmpty()) {
            return "";
        } else {
            return "?" + StringUtils.mapJoin(
                    parameters,
                    pair -> pair.getLeft() + "=" + encode(pair.getRight()),
                    "&"
            );
        }
    }

    public static String encode(String url) {
        return URLEncoder.encode(url, Charset.forName("UTF-8"));
    }

}

