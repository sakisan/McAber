package be.sakisan.mcaber.utils;

import java.util.List;
import java.util.function.Function;

public class StringUtils {

    private StringUtils() {
        throw new java.lang.IllegalStateException("Utility class");
    }

    public static boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public static boolean isNotEmpty(String string) {
        return !isEmpty(string);
    }

    public static String join(Iterable<String> list, String separator) {
        return PureUtils.unifold((next, string) -> string + separator + next, list).orElse("");
    }

    public static <T> String mapJoin(List<T> list, Function<T, String> toString, String sep) {
        return join(ListUtils.map(list, toString), sep);
    }

}

