package be.sakisan.mcaber.utils;

import java.util.Optional;
import java.util.function.BiFunction;

public class PureUtils {

    private PureUtils() {
        throw new java.lang.IllegalStateException("Utility class");
    }

    public static <T, R> R fold(BiFunction<T, R, R> folder, R initial, Iterable<T> list) {
        R result = initial;
        for (T member : list) {
            result = folder.apply(member, result);
        }
        return result;
    }

    public static <R> Optional<R> unifold(BiFunction<R, R, R> folder, Iterable<R> list) {
        Optional<R> result = Optional.empty();
        for (R member : list) {
            if (result.isEmpty()) {
                result = Optional.of(member);
            } else {
                result = result.map(r -> folder.apply(member, r));
            }
        }
        return result;
    }

}
