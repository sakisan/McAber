package be.sakisan.mcaber.utils;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ListUtils {

    private ListUtils() {
        throw new java.lang.IllegalStateException("Utility class");
    }

    public static <I, O> List<O> map(List<I> input, Function<I, O> function) {
        return input.stream().map(function).collect(Collectors.toList());
    }

}

