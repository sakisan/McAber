package be.sakisan.mcaber.data;

import java.util.Optional;

/**
 * Copy this interface and use an interface that extends Spec
 * to limit which kind of specs are applicable.
 *
 * We can't make Spec generic here because it has a generic type of its own.
 * But that's not really an issue.
 */
public interface Data {

    <D> Optional<D> getData(Spec<D> spec);

}

