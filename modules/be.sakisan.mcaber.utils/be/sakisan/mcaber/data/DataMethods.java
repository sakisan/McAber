package be.sakisan.mcaber.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@SuppressWarnings("unchecked")
public class DataMethods {
    private final Map methods = new HashMap();

    public <D, V extends Spec<D>> DataMethods optional(Class<V> clazz, Function<V, Optional<D>> method) {
        this.methods.put(clazz, method);
        return this;
    }

    public <D, V extends Spec<D>> DataMethods nullable(Class<V> clazz, Function<V, D> method) {
        this.methods.put(clazz, method.andThen(Optional::ofNullable));
        return this;
    }

    public <D, V extends Spec<D>> Optional<D> applyMethod(V spec) {
        Function<V, Optional<D>> method
                = (Function<V, Optional<D>>) methods.get(spec.getClass());
        if (method != null) {
            return method.apply(spec);
        } else {
            return Optional.empty();
        }
    }
}
