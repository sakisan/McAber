# Location of trees.
SOURCE_DIR  := modules
OUTPUT_DIR  := classes
DOCKER_PATH := /usr/src/myapp
LIBS        := libs

PACKAGE     := be.sakisan.mcaber
MAIN_CLASS  := $(PACKAGE).Main

# unix tools
FIND        := /usr/bin/find

DOCKER      := docker run --rm                                       \
                -u `id -u`                                           \
                -v "$$PWD":$(DOCKER_PATH)                            \
                -w $(DOCKER_PATH)                                    \
		--network host                                       \
                openjdk:11 
DOCKER_I    := docker run --rm -i                                    \
                -u `id -u`                                           \
                -v "$$PWD":$(DOCKER_PATH)                            \
                -w $(DOCKER_PATH)                                    \
		--network host                                       \
                openjdk:11 
JAVAC       := $(DOCKER) javac                                       \
                -d $(OUTPUT_DIR)                                     \
		-Xlint:unchecked                                     \
                --module-path $(LIBS)                                \
                --module-source-path $(SOURCE_DIR)
JAVA        := $(DOCKER) java                                        \
                --module-path $(OUTPUT_DIR):$(LIBS)
JAR         := $(DOCKER) jar --create

$(PACKAGE).jar: compile
	for module in utils bot discord osu osubot osurest ; do      \
	    $(JAR) --file $(LIBS)/$(PACKAGE).$$module.jar            \
	     	   -C $(OUTPUT_DIR)/$(PACKAGE).$$module . ;          \
	done
	$(JAR)  --file $(LIBS)/$(PACKAGE).jar                        \
                --main-class $(MAIN_CLASS)                           \
                -C $(OUTPUT_DIR)/$(PACKAGE) .

# all_javas - Temp file for holding source file list
all_javas := $(OUTPUT_DIR)/all.javas

.PHONY: compile
compile: libs $(all_javas)
	$(JAVAC) @$(all_javas)

.INTERMEDIATE: $(all_javas)
$(all_javas):
	rm -rf $(OUTPUT_DIR)
	mkdir -p $(OUTPUT_DIR)
	$(FIND) $(SOURCE_DIR) -name '*.java' > $@

libs:
	mkdir libs
	wget -O libs/JDA-3.8.3_462.jar https://github.com/DV8FromTheWorld/JDA/releases/download/v3.8.3/JDA-3.8.3_462-withDependencies-no-opus.jar

.PHONY: bounce
bounce: console

.PHONY: console
console: compile
	$(DOCKER_I) java                                             \
                --module-path $(OUTPUT_DIR):$(LIBS)                  \
	        --module $(PACKAGE)/$(MAIN_CLASS) test

.PHONY: help
help:
	$(JAVA) -h
	# $(DOCKER) jlink -h
	# $(DOCKER) jar -h
	# $(DOCKER) jshell -h

